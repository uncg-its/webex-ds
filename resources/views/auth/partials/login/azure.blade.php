<div class="card p-3">
    <h5 class="card-title">Azure</h5>
    <div class="card-body text-center">
        <a href="{{ route('oauth', ['provider' => 'azure']) }}" class="btn btn-primary btn-success" title="Login with Azure">
            <i class="fab fa-windows mr-1"></i> Azure
        </a>
    </div>
</div>