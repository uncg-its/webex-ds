@extends('layouts.wrapper', [
    'pageTitle' => 'Delegates | Manage'
])

@section('content')
    @include('delegates.partials.errors')
    @include('delegates.partials.env')

    <div class="row">
        <div class="col-10 offset-1">
            {!! Form::open()->route('delegates.preview') !!}
            {!! Form::fieldsetOpen('Manage Delegates') !!}
                {!! Form::text('hosts', 'Host Webex IDs')->help('User(s) on which to perform the operation (comma-separated)') !!}
                {!! Form::select('transaction', 'Transaction type', ['add' => 'Add Delegates', 'remove' => 'Remove Delegates', 'sync' => 'Sync Delegates'])->help('Note: \'sync\' will overwrite the users\' current delegates list with the names you provide. Use with caution!') !!}
                {!! Form::text('delegates', 'Delegate Webex IDs')->help('Delegate(s) to use with the above operation (comma-separated)') !!}
                {!! Form::submit('Submit request') !!}
            {!! Form::fieldsetClose() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection()
