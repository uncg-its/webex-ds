<div class="row">
    <div class="col">
        <div class="alert alert-secondary">
            <p class="mb-0"><strong>Current Webex Environment</strong>: <code>{{ $webexApiEnv }}</code> ({{ config('delegated-scheduler.environments.' . $webexApiEnv . '.xml_host') }})</p>
        </div>
    </div>
</div>