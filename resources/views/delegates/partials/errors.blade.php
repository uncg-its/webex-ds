@if($errors->any())
    <div class="alert alert-danger">
        <i class="fas fa-exclamation-triangle mr-1"></i>
        Your form submission contained errors. Please see below.
    </div>
@elseif($errors->api->any())
    <div class="alert alert-danger">
        <i class="fas fa-exclamation-triangle mr-1"></i>
        API Error: {{ implode('; ', $errors->api->all()) }}
    </div>
@endif