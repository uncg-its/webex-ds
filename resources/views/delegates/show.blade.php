@extends('layouts.wrapper', [
    'pageTitle' => 'Delegates | Show'
])

@section('content')
    @include('delegates.partials.errors')
    @include('delegates.partials.env')

    <h1>Show Delegates</h1>
    <div class="row">
        <div class="col-10 offset-1">
            {!! Form::open() !!}
            {!! Form::fieldsetOpen('Show Delegates') !!}
                {!! Form::text('hosts', 'Host Webex IDs')->help('User(s) for which to look up existing delegates (comma-separated)') !!}
                {!! Form::submit('Submit request') !!}
            {!! Form::fieldsetClose() !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection()
