@extends('layouts.wrapper', [
    'pageTitle' => 'Delegates | Show'
])

@section('content')
    <h1>Show Delegates</h1>
    @include('delegates.partials.env')
    <div class="row">
        <div class="col">
            @if(count($delegatesByUser) > 0)
                <table class="table table-sm table-hover">
                    <thead class="thead-light">
                    <tr>
                        <th>Webex ID</th>
                        <th>Current Delegates</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($delegatesByUser as $webexId => $delegates)
                        <tr>
                            <td>{{ $webexId }}</td>
                            <td>{!! count($delegates) > 0 ? implode(', ', $delegates) : "<i>No delegates.</i>" !!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <p>No results.</p>
            @endif

            <p><a href="{{ URL::previous() }}" class="btn btn-sm btn-primary"><i class="fas fa-arrow-left"></i> Search again</a></p>
        </div>
    </div>
@endsection()