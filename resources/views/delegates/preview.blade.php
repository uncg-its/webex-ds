@extends('layouts.wrapper', [
    'pageTitle' => 'Delegates | Preview'
])

@section('content')
    <h1>Preview results</h1>
    <div class="row">
        <div class="col">
            <p>The following actions will be taken:</p>
            <table class="table table-sm table-hover">
                <thead class="thead-light">
                <tr>
                    <th>Username</th>
                    <th>Current Delegates</th>
                    <th>Action</th>
                    <th>List</th>
                    <th>New Delegates</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                    <tr>
                        <td>{{ $result['user'] }}</td>
                        <td>{{ implode(',', $result['current']) }}</td>
                        <td>{{ $result['action'] }}</td>
                        <td>{{ implode(',', $result['list']) }}</td>
                        <td>{{ implode(',', $result['new']) }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {!! Form::open()->route('delegates.manage') !!}
            <input type="hidden" name="unique_id" value="{{ $uniqueId }}">
            {!! Form::submit('Confirm') !!}
            {!! Form::close() !!}
        </div>
    </div>
@endsection()