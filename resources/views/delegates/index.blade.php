@extends('layouts.wrapper', [
    'pageTitle' => 'Delegates | Index'
])

@section('content')

    @include('delegates.partials.env')
    <h1>Delegate Management</h1>
    <div class="row">
        @permission('delegates.view')
            @include('components.panel-nav', [
                'url' => route('delegates.show'),
                'fa' => 'fas fa-search',
                'title' => 'Show Delegates'
            ])
        @endpermission

        @permission('delegates.manage')
            @include('components.panel-nav', [
                'url' => route('delegates.manage'),
                'fa' => 'fas fa-cogs',
                'title' => 'Manage Delegates'
            ])
        @endpermission
    </div>
@endsection()
