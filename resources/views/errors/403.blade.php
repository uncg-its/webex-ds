@extends('layouts.wrapper', [
    'pageTitle' => '403'
])

@section('content')
    <h2>403 - Forbidden</h2>
    <div class="alert alert-danger">
        <i class="fas fa-exclamation-triangle"></i>
        <strong>Error:</strong> Permission denied. <em>{{ $exception->getMessage() }}</em>
    </div>
    <p><a href="{{ back()->getTargetUrl() }}">Go Back</a></p>

@endsection()