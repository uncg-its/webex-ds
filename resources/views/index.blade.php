@extends('layouts.wrapper', [
    'pageTitle' => 'Home'
])

@section('content')
    <h1>{{ config('app.name') }}</h1>
    <div class="row">
        @if($user)
            @role('admin')
                @include('components.panel-nav', [
                    'url' => route('admin'),
                    'fa' => 'fas fa-key',
                    'title' => 'Administrator'
                ])
            @endrole

            @foreach($modules as $key => $module)
                @if(empty($module['parent']))
                    @permission($module['required_permissions'])
                    @include('components.panel-nav', [
                        'url' => route($module['index']),
                        'fa' => $module['icon'],
                        'title' => $module['title']
                    ])
                    @endpermission
                @endif
            @endforeach
            
            @permission('delegates.*')
                @include('components.panel-nav', [
                    'url' => route('delegates.index'),
                    'fa' => 'fas fa-users',
                    'title' => 'Manage Delegates'
                ])
            @endpermission

        @else
            <div class="col-md-12">
                <p>You are not logged in - please <a href="{{ route('login') }}">log in</a> to proceed.</p>
            </div>
        @endif
    </div>
@endsection