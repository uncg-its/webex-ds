<nav class="navbar navbar-dark navbar-expand-lg bg-dark justify-content-between mb-3">
    <a class="navbar-brand" href="{{ config('app.url') }}">{{ config('app.name') }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            @if(isset($nav))
                @include('components.bs4-menu-items', array('items' => $nav->roots()))
            @endif
        </ul>
        <div>
            @if(env('APP_SEARCH'))
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            @endif
            @if($user)
                <p class="navbar-text mb-0">
                    <a href="{{ route('account') }}">{{ $user->email }}</a>
                </p>
            @else
                <p class="navbar-text mb-0 mr-3"><a href="{{ route('login') }}">Log in</a></p>
                @if(config('ccps.allow_signups') && in_array('local', config('ccps.login_methods')))
                    <p class="navbar-text mb-0"><a href="{{ route('register') }}">Register</a></p>
                @endif
            @endif
        </div>
    </div>
</nav>