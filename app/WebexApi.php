<?php


namespace App;

use Uncgits\WebexApiLaravel\Exceptions\WebexApiException;
use Uncgits\WebexApiLaravel\WebexApi as BaseApiModel;

class WebexApi extends BaseApiModel {

    public function __construct($environment = null) {
        parent::__construct();

        if (is_null($environment)) {
            $environment = $this->getEnvironment();
        }

        $definedEnvironments = config('delegated-scheduler.environments');
        if (!isset($definedEnvironments[$environment])) {
            throw new \Exception('Webex API Environment "' . $environment . '" is not registered in the configuration.');
        }

        // re-set creds based on environment
        $this->setXmlHost(config('delegated-scheduler.environments.' . $environment . '.xml_host'));
        $this->setNbrHost(config('delegated-scheduler.environments.' . $environment . '.nbr_host'));
        $this->setUsername(config('delegated-scheduler.environments.' . $environment . '.username'));
        $this->setPassword(config('delegated-scheduler.environments.' . $environment . '.password'));
        $this->setSiteName(config('delegated-scheduler.environments.' . $environment . '.site_name'));
        $this->setPartnerId(config('delegated-scheduler.environments.' . $environment . '.partner_id'));
    }

    public function getEnvironment() {
        $environment = data_get(app('dbConfig'), 'environment', null);

        if (is_null($environment)) {
            $environment = config('delegated-scheduler.default_environment');
        }

        return $environment;
    }


    public function syncSchedulingPermissionsForUser($username) {
        $getUserRequest = $this->getUser($username);
        $user = $getUserRequest['data'];
        $oldPermissions = $user['use:schedulingPermission'];
        dd($oldPermissions);
    }

    /**
     * @param $username
     *
     * @return array
     * @throws WebexApiException
     */
    public function getSchedulingDelegatesForUser($username) {
        $getUserRequest = WebexApi::getUser($username);
        if ($getUserRequest['response']['result'] != 'SUCCESS') {
            throw new WebexApiException($getUserRequest, 'user: ' . $username);
        }

        $currentDelegates = $getUserRequest['data']['use:schedulingPermission'] ?? '';
        return $currentDelegates == '' ? [] : explode(';', $currentDelegates);
    }


    /**
     * @param $username
     * @param $delegates
     *
     * @return array
     * @throws WebexApiException
     */
    public function addSchedulingDelegatesForUser($username, $delegates, $pretend = false) {
        $currentDelegates = $this->getSchedulingDelegatesForUser($username);

        foreach($delegates as $user) {
            $getUserRequest = WebexApi::getUser($user);
            if ($getUserRequest['response']['result'] != 'SUCCESS') {
                throw new WebexApiException($getUserRequest, 'user: ' . $user);
            }
        }

        $finalDelegatesList = array_unique(array_merge($currentDelegates, $delegates));

        if ($pretend) {
            return [
                'user' => $username,
                'current' => $currentDelegates,
                'action' => 'add',
                'list' => $delegates,
                'new' => $finalDelegatesList
            ];
        }

        return $this->setUserSchedulingPermission($username, $finalDelegatesList);
    }

    /**
     * @param $username
     * @param $delegates
     *
     * @return array
     * @throws WebexApiException
     */
    public function removeSchedulingDelegatesForUser($username, $delegates, $pretend = false) {
        $currentDelegates = $this->getSchedulingDelegatesForUser($username);
        $finalDelegatesList = array_diff($currentDelegates, $delegates);

        if ($pretend) {
            return [
                'user' => $username,
                'current' => $currentDelegates,
                'action' => 'remove',
                'list' => $delegates,
                'new' => $finalDelegatesList
            ];
        }

        return $this->setUserSchedulingPermission($username, $finalDelegatesList);
    }

    /**
     * @param $username
     * @param $delegates
     *
     * @return array
     */
    public function syncSchedulingDelegatesForUser($username, $delegates, $pretend = false) {
        $currentDelegates = $this->getSchedulingDelegatesForUser($username);
        $finalDelegatesList = $delegates;

        if ($pretend) {
            return [
                'user' => $username,
                'current' => $currentDelegates,
                'action' => 'sync',
                'list' => $delegates,
                'new' => $finalDelegatesList
            ];
        }

        return $this->setUserSchedulingPermission($username, $finalDelegatesList);
    }
}