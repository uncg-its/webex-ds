<?php

namespace App\Listeners;

use App\Events\ManageQuerySubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogManageQuery
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ManageQuerySubmitted  $event
     * @return void
     */
    public function handle(ManageQuerySubmitted $event)
    {
        $details = [
            'hosts' => $event->request->hosts,
            'transaction' => $event->request->transaction,
            'delegates' => $event->request->delegates
        ];

        Log::channel('queries')->info('MANAGE query submitted by ' . $event->user->email . ': ' . json_encode($details));
    }
}
