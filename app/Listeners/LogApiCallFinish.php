<?php

namespace App\Listeners;

use Uncgits\WebexApiLaravel\Events\ApiCallFinished;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogApiCallFinish
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiCallFinished  $event
     * @return void
     */
    public function handle(ApiCallFinished $event)
    {
        Log::channel('webex-api')->info('API call finished: ' . json_encode($event->callMeta));
    }
}
