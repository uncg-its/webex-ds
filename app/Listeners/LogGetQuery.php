<?php

namespace App\Listeners;

use App\Events\GetQuerySubmitted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogGetQuery
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  GetQuerySubmitted  $event
     * @return void
     */
    public function handle(GetQuerySubmitted $event)
    {
        $details = [
            'hosts' => $event->request->hosts
        ];

        Log::channel('queries')->info('GET query submitted by ' . $event->user->email . ': ' . json_encode($details));
    }
}
