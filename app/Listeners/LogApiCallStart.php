<?php

namespace App\Listeners;

use Uncgits\WebexApiLaravel\Events\ApiCallStarted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class LogApiCallStart
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApiCallStarted  $event
     * @return void
     */
    public function handle(ApiCallStarted $event)
    {
        Log::channel('webex-api')->info('API call started: ' . json_encode($event->callMeta));
    }
}
