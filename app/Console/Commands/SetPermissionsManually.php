<?php

namespace App\Console\Commands;

use App\WebexApi;
use Illuminate\Console\Command;

class SetPermissionsManually extends Command
{
    private $api;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ds:manual {webexId} {list}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sets a user\'s delegated scheduling list manually.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->api = app('WebexApi');

        $this->api->setUserSchedulingPermission($this->argument('webexId'), explode(';', $this->argument('list')));
    }
}
