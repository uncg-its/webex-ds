<?php

namespace App\Http\Controllers;

use App\Events\GetQuerySubmitted;
use App\Events\ManageQuerySubmitted;
use App\Transaction;
use Carbon\Carbon;
use Ramsey\Uuid\Uuid;
use Uncgits\WebexApiLaravel\Exceptions\WebexApiException;
use Uncgits\WebexApiLaravel\Rules\AllActiveWebexUsers;
use Uncgits\WebexApiLaravel\Facades\WebexApi;
use Illuminate\Http\Request;

class DelegateController extends Controller
{
    public function __construct() {
        $this->middleware('permission:delegates.*')->only(['index']);
        $this->middleware('permission:delegates.view')->only(['get', 'show']);
        $this->middleware('permission:delegates.manage')->only(['manage', 'process']);
    }

    public function index() {
        return view('delegates.index');
    }

    public function show() {
        return view('delegates.show');
    }

    public function manage() {
        return view('delegates.manage');
    }

    public function get(Request $request) {
        event(new GetQuerySubmitted(auth()->user(), $request));
        $validatedData = $request->validate([
            'hosts' => [
                'required',
                new AllActiveWebexUsers()
            ]
        ]);

        $delegatesByUser = [];
        $users = explode(',', data_get($validatedData, 'hosts'));

        try {
            foreach ($users as $user) {
                $delegatesByUser[$user] = WebexApi::getSchedulingDelegatesForUser($user);
            }

            return view('delegates.get')->with(compact('delegatesByUser'));
        } catch (WebexApiException $e) {
            // catch API problem
            $message = $e->response['response']['message'];
            $endpoint = $e->response['response']['endpoint'];

            $more = !empty($e->more) ? " ($e->more)" : '';

            $exceptionMessage = "During call to {$endpoint}{$more}: $message";

            return redirect()->back()->withErrors($exceptionMessage, 'api');
        } catch (\Exception $e) {
            throw $e;
        }
    }
    
    public function preview(Request $request) {
        event(new ManageQuerySubmitted(auth()->user(), $request));

        $validatedData = $request->validate([
            'hosts'       => [
                'bail',
                'required',
                new AllActiveWebexUsers()
            ],
            'transaction' => 'required|in:add,remove,sync',
            'delegates'   => [
                'bail',
                'required',
                new AllActiveWebexUsers()
            ],
        ]);

        // which transaction?
        $transaction = data_get($validatedData, 'transaction');
        $transactionMethod = $transaction . 'SchedulingDelegatesForUser';

        try {
            $hosts = explode(',', data_get($validatedData, 'hosts', []));
            $delegates = explode(',', data_get($validatedData, 'delegates', []));

            $results = [];

            foreach ($hosts as $host) {
                $results[] = WebexApi::$transactionMethod($host, $delegates, true);
            }

            // store transaction in database with unique id
            $uniqueId = Uuid::uuid1()->toString();

            Transaction::create([
                'unique_id' => $uniqueId,
                'transaction' => json_encode($results)
            ]);

            return view('delegates.preview')->with([
                'results' => $results,
                'uniqueId' => $uniqueId
            ]);

        } catch (\Exception $e) {
            flash('There was an error: ' . $e->getMessage(), 'danger');
            return redirect()->back();
        }

    }

    
    public function process(Request $request) {

        $validatedData = $request->validate([
            'unique_id' => 'required'
        ]);

        try {
            $transaction = Transaction::findOrFail(data_get($validatedData, 'unique_id'));

            $actions = json_decode(data_get($transaction, 'transaction'));

            foreach($actions as $action) {
                $hosts = data_get($action, 'user');
                $delegates = data_get($action, 'list', []);

                // which transaction?
                $transactionType = data_get($action, 'action');
                $transactionMethod = $transactionType . 'SchedulingDelegatesForUser';

                WebexApi::$transactionMethod($hosts, $delegates);
            }

            // update transaction to be completed.
            $transaction->update([
                'completed_at' => Carbon::now()->toDateTimeString()
            ]);

            flash('Successfully edited delegates.', 'success');
            return redirect()->route('delegates.manage');

        } catch (WebexApiException $e) {
            // catch API problem
            $message = $e->response['response']['message'];
            $endpoint = $e->response['response']['endpoint'];

            $more = !empty($e->more) ? " ($e->more)" : '';

            $exceptionMessage = "During call to {$endpoint}{$more}: $message";

            return redirect()->back()->withErrors($exceptionMessage, 'api');
        } catch (\Exception $e) {
            throw $e;
        }

        return redirect()->back();
    }
}
