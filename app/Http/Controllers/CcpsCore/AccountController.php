<?php

namespace App\Http\Controllers\CcpsCore;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Uncgits\Ccps\Controllers\AccountController as BaseController;

class AccountController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function settings()
    {
        $skins = config('ccps.bootstrap_skins');
        $selected = auth()->user()->settings->skin ?? 'default';

        return view('account.settings')->with([
            'skins'    => $skins,
            'selected' => $selected
        ]);
    }

    public function updateSettings(Request $request)
    {
        $approvedSettings = ['skin'];
        $skins = array_keys(config('ccps.bootstrap_skins'));

        $validated = $request->validate([
            'skin' => [
                'required',
                Rule::in($skins),
            ]
        ]);

        $user = auth()->user();

        $existingSettings = $user->settings ?? new \stdClass();
        foreach ($approvedSettings as $setting) {
            $existingSettings->{$setting} = $request->$setting;
        }

        try {
            $user->settings = json_encode($existingSettings);
            $user->save();

            flash('Settings updated!', 'success');
        } catch (\Exception $e) {
            \Log::channel('general')->error('Error while updating settings for user ' . $user->id . ': ' . $e->getMessage());
            flash('There was an error updating your account settings. Please contact an administrator.');
        }
        return redirect()->back();
    }
}
