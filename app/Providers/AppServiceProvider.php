<?php

namespace App\Providers;

use App\CcpsCore\DbConfig;
use App\WebexApi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Symfony\Component\Console\Output\ConsoleOutput;
use Uncgits\WebexApiLaravel\Facades\WebexApi as WebexApiFacade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // view vars
        View::composer('delegates.*', function($view) {
            $webexApiEnv = WebexApiFacade::getEnvironment();
            $view->with(compact('webexApiEnv'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // bind our extended object into container in place of the one from the package.
        $this->app->singleton('WebexApi', function($app) {
            return new WebexApi();
        });
    }
}
