<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $guarded = [];
    public $incrementing = false;
    public $timestamps = false;
    protected $primaryKey = 'unique_id';
}
