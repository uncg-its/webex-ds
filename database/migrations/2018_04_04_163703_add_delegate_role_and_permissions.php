<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Artisan;

use App\CcpsCore\Permission;
use App\CcpsCore\Role;

class AddDelegateRoleAndPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\DelegateRolesTableSeeder',
            '--force' => true
        ]);

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\DelegatePermissionsTableSeeder',
            '--force' => true
        ]);

        Artisan::call('db:seed', [
            '--class' => 'App\\Seeders\\DelegatePermissionRoleTableSeeder',
            '--force' => true
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $permissions = Permission::whereIn('name', ['delegates.manage', 'delegates.view'])->get();
        $roles = Role::whereIn('name', ['admin', 'delegate-manager'])->get();

        foreach($roles as $role) {
            $role->detachPermissions($permissions);
            if ($role->editable) {
                $role->delete();
            }
        }

        foreach($permissions as $permission) {
            $permission->delete();
        }
    }
}
