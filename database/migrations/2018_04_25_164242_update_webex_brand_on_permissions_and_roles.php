<?php

use App\CcpsCore\Permission;
use App\CcpsCore\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWebexBrandOnPermissionsAndRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function() {
            $role = Role::where('name', 'delegate-manager')->firstOrFail();
            $role->update([
                'display_name' => str_replace('WebEx', 'Webex', $role->display_name),
                'description' => str_replace('WebEx', 'Webex', $role->description),
            ]);

            $permission = Permission::where('name', 'delegates.view')->firstOrFail();
            $permission->update([
                'description' => str_replace('WebEx', 'Webex', $permission->description),
            ]);

            $permission2 = Permission::where('name', 'delegates.manage')->firstOrFail();
            $permission2->update([
                'description' => str_replace('WebEx', 'Webex', $permission2->description),
            ]);
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function() {
            $role = Role::where('name', 'delegate-manager')->firstOrFail();
            $role->update([
                'display_name' => str_replace('Webex', 'WebEx', $role->display_name),
                'description' => str_replace('Webex', 'WebEx', $role->description),
            ]);

            $permission = Permission::where('name', 'delegates.view')->firstOrFail();
            $permission->update([
                'description' => str_replace('Webex', 'WebEx', $permission->description),
            ]);

            $permission2 = Permission::where('name', 'delegates.manage')->firstOrFail();
            $permission2->update([
                'description' => str_replace('Webex', 'WebEx', $permission2->description),
            ]);
        });
    }
}
