<?php

return [

    'default_environment' => env('WEBEX_API_DEFAULT_ENVIRONMENT', 'test'),

    'environments' => [
        'production' => [
            // META
            'display_name' => 'Production (faculty/staff)',

            // URLs
            'xml_host' => env('WEBEX_API_XML_HOST'),
            'nbr_host' => env('WEBEX_API_NBR_HOST'),

            // CREDENTIALS
            'username' => env('WEBEX_API_USERNAME'),
            'password' => env('WEBEX_API_PASSWORD'),
            'site_name' => env('WEBEX_API_SITE_NAME'),
            'partner_id' => env('WEBEX_API_PARTNER_ID'),
        ],

        'students' => [
            // META
            'display_name' => 'Students',

            // URLs
            'xml_host' => env('WEBEX_API_STUDENTS_XML_HOST'),
            'nbr_host' => env('WEBEX_API_STUDENTS_NBR_HOST'),

            // CREDENTIALS
            'username' => env('WEBEX_API_STUDENTS_USERNAME'),
            'password' => env('WEBEX_API_STUDENTS_PASSWORD'),
            'site_name' => env('WEBEX_API_STUDENTS_SITE_NAME'),
            'partner_id' => env('WEBEX_API_STUDENTS_PARTNER_ID'),
        ],

        'test' => [
            // META
            'display_name' => 'Test',

            // URLs
            'xml_host' => env('WEBEX_API_TEST_XML_HOST'),
            'nbr_host' => env('WEBEX_API_TEST_NBR_HOST'),

            // CREDENTIALS
            'username' => env('WEBEX_API_TEST_USERNAME'),
            'password' => env('WEBEX_API_TEST_PASSWORD'),
            'site_name' => env('WEBEX_API_TEST_SITE_NAME'),
            'partner_id' => env('WEBEX_API_TEST_PARTNER_ID'),
        ]
    ],

];